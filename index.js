var config = require('./config.json');

var ActionsDispatcher = require('./actions-dispatcher');
var dispatcher = new ActionsDispatcher(casper);

dispatcher.addAction('screenshot');
dispatcher.addAction('wait');
dispatcher.addAction('hide-block');

dispatcher.resetPages(config.pages);
for (var i = 0; i < config.viewports.length; i++) {
    var viewport = config.viewports[i];
    dispatcher.start(viewport[0], viewport[1]);
}
