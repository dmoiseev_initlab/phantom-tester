var WaitAction = function (casper) {
    this.casper = casper;
};

WaitAction.prototype.start = function (time) {

    this.casper.wait(time);

};

module.exports = WaitAction;
