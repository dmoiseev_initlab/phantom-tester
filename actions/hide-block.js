var HideAction = function (casper) {
    this.casper = casper;
};

HideAction.prototype.start = function (selector) {

    this.casper.thenEvaluate(function (selector) {
        var element = document.querySelector(selector);
        if (element) {
            element.style.opacity = 0;
        }
    }, selector);

};

module.exports = HideAction;
