var ScreenshotAction = function (casper, phantomcss) {
    this.casper = casper;
    this.phantomcss = phantomcss;
};

ScreenshotAction.prototype.start = function (selector) {
    var phantomcss = this.phantomcss;

    this.casper.then(function () {
        phantomcss.screenshot(selector, 'screenshot');
    });

};

module.exports = ScreenshotAction;
