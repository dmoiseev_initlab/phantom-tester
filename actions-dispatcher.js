var config = require('./config.json');
var fs = require('fs');
var system = require('system');

var login;
var password;
var dir_path;
var phantomcss;

if (config.hasOwnProperty('auth') && config.auth.length > 1) {
  login = config.auth[0];
  password = config.auth[1];
}
if (system.env.PHANTOM_HTTP_LOGIN && system.env.PHANTOM_HTTP_PASSWORD) {
  login = system.env.PHANTOM_HTTP_LOGIN;
  password = system.env.PHANTOM_HTTP_PASSWORD;
}

/**
 * ActionDispatcher constructor.
 * Also creates phantomcss instance.
 */
var ActionsDispatcher = function (casper) {
    this.casper = casper;

    this.actions = {};
    this.pages = [];

    var currentFile = casper.test.currentTestFile;
    var curFilePath = fs.absolute(currentFile).split('/');

    if (curFilePath.length) {
        curFilePath.pop();
        dir_path = curFilePath.join('/') + '/';
    }
    else {
        dir_path = './';
    }
    var path = dir_path + 'node_modules/phantomcss/phantomcss.js';
    var system = require('system');

    phantomcss = require( path );

    phantomcss.init({
        rebase: casper.cli.get( "rebase" ),
        casper: casper,
        libraryRoot: dir_path + 'node_modules/phantomcss',
        screenshotRoot: dir_path + '/screenshots',
        failedComparisonsRoot : dir_path + '/failures',
        addLabelToFailedImage : false,
        cleanupComparisonImages: true,
    });
};

/**
 * Add new action to dispatcher.
 * @param name
 */
ActionsDispatcher.prototype.addAction = function (name) {
    var action = require('./actions/' + name);
    this.actions[name] = new action(this.casper, phantomcss);
};

/**
 * Replace current pages with pages from array.
 * @param pages
 */
ActionsDispatcher.prototype.resetPages = function (pages) {
    this.pages = pages;
};

/**
 * Process 1 action.
 * @param action
 * @param params
 */
ActionsDispatcher.prototype.processAction = function (action, params) {
    if (this.actions[action]) {
        this.actions[action].start(params);
    }
};

/**
 * Process page.
 * @param page
 */
ActionsDispatcher.prototype.processPage = function (page) {
    var self = this;

    self.casper.test.begin('Processing ' + page.name, function (test) {
        self.casper.start(config.baseUrl + page.url);

        if (config.before) {
            for (var i = 0; i < config.before.length; i++) {
                self.processAction(config.before[i].type, config.before[i].params);
            }
        }

        for (var i in page.actions) {
            self.processAction(page.actions[i].type, page.actions[i].params);
        }

        self.casper.run(function () {
            test.done();
        })
    });
};

/**
 * Prepare environment for screenshoting.
 */
ActionsDispatcher.prototype.prepare = function (width, height) {
    var casper = this.casper;

    casper.test.begin('Change viewport size to ' + width + 'x' + height, function (test) {
        casper.start(config.baseUrl);
        if (login && password) {
          casper.setHttpAuth(login, password);
        }

        casper.then(function () {
            casper.viewport(width, height);
            var basePath = dir_path + '/';
            var resolution = width + '_' + height;

            phantomcss.update({
                screenshotRoot: basePath + 'screenshots/' + resolution,
                failedComparisonsRoot: basePath + 'failures/' + resolution,
                comparisonResultRoot: basePath + 'screenshots/' + resolution
            });
        });

        casper.run(function () {
            test.done();
        });
    });
};

ActionsDispatcher.prototype.compareScreenshots = function () {
    var casper = this.casper;

    casper.test.begin('Compare screenshots', function suite(test) {
        casper.then(function (){
            phantomcss.compareAll();
        });

        casper.run(function () {
            test.done();
        });
    });
};

ActionsDispatcher.prototype.start = function (width, height) {
    this.prepare(width, height);

    var pageCount = this.pages.length;

    for (var i = 0; i < pageCount; i++) {
        var page = this.pages[i];
        this.processPage(page);
    }

    this.compareScreenshots();
};

module.exports = ActionsDispatcher;
